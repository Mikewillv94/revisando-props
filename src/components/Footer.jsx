import React from "react";
import FooterNote from "./FooterNote";

class Footer extends React.Component {
  render() {
    return (
      <div>
        {this.props.text}
        <div>
          <FooterNote text="Repetir mais!"></FooterNote>
          <FooterNote
            text="Repetição para praticar!"
            feeling={this.props.feeling}
          ></FooterNote>
        </div>
      </div>
    );
  }
}
export default Footer;
