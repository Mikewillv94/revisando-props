import React from "react";
import TitleNote from "./TitleNote";

class Title extends React.Component {
  render() {
    return (
      <h1>
        {this.props.text}
        <div>
          <TitleNote text="Repetir mais ainda!"></TitleNote>
          <TitleNote
            text="Aprendendo React"
            feeling={this.props.feeling}
          ></TitleNote>
        </div>
      </h1>
    );
  }
}
export default Title;
