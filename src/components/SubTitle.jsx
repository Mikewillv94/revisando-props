import React from "react";
import SubTitleNote from "./SubTitleNote";

class SubTitle extends React.Component {
  render() {
    return (
      <h2>
        {this.props.text}
        <div>
          <SubTitleNote text="Manda mais repetição!"></SubTitleNote>
          <SubTitleNote
            text="Exercício sobre props."
            feeling={this.props.feeling}
          ></SubTitleNote>
        </div>
      </h2>
    );
  }
}
export default SubTitle;
