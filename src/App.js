import "./App.css";
import Title from "./components/Title";
import SubTitle from "./components/SubTitle";
import Body from "./components/Body";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Title text="Aprendendo React" feeling="<3"></Title>
        <SubTitle text="Exercício sobre props." feeling=" <3"></SubTitle>
        <Body text="Prática leva a perfeição!" feeling=" <3"></Body>
        <Footer text="Repetição para praticar!" feeling=" <3"></Footer>
      </header>
    </div>
  );
}
export default App;
